ThisBuild / scalaVersion := "3.2.2"

val eventStore = (project in file ("./event-store/"))
   .settings(moduleName := "event-store")
   .settings(
      libraryDependencies ++= Seq(
         Deps.http4sDsl,
         Deps.http4sEmberServer,
         Deps.http4sEmberClient,
         Deps.catsEffect
      )
   )
