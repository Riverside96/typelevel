import sbt._
object Deps{

  private val http4sVersion = "0.23.19"
  private val catsEffectVersion = "3.5.0"
  
private def http4s(branch: String) = "org.http4s" %% s"http4s-$branch" % http4sVersion
  
  val http4sDsl         = http4s("dsl")
  val http4sEmberServer = http4s("ember-server")
  val http4sEmberClient = http4s("ember-client")

  val catsEffect = "org.typelevel" %% "cats-effect" % catsEffectVersion

}
